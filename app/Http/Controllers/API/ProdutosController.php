<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Produto;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    public function buscaTodos(){
        return Produto::all();
    }
}
